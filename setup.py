#!/usr/bin/env python

import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

packages = ['kombu']

requires = []

about = {}
with open(os.path.join(here, 'kombu', '__version__.py'), 'r') as f:
    exec(f.read(), about)

# The text of the README file
with open(os.path.join(here, 'README.md'), 'r') as f:
    readme = f.read()

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme,
    long_description_content_type="text/markdown",
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    packages=packages,
    package_data={'': ['LICENSE', 'NOTICE'], 'kombu': ['async_/*.py', 'tests/*.py', 'tests/async/*.py', 
                                                       'tests/transport/*.py', 'tests/transport/virtual/*.py', 
                                                       'tests/utils/*.py', 'transport/*.py', 'transport/django/*.py', 
                                                       'transport/django/management/*.py', 
                                                       'transport/django/management/commands/*.py', 
                                                       'transport/django/migrations/*.py', 
                                                       'transport/django/south_migrations/*.py', 
                                                       'transport/sqlalchemy/*.py', 'transport/virtual/*.py', 
                                                       'utils/*.py']},
    package_dir={'kombu': 'kombu'},
    include_package_data=True,
    python_requires=">=3.6.*",
    install_requires=requires,
    license=about['__license__'],
    zip_safe=False,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
    project_urls={
        'Documentation': 'https://gitlab.com/Fer12/kombu',
        'Source': 'https://gitlab.com/Fer12/kombu',
    },
)
